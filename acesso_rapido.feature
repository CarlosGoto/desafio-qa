Funcionalidade:
    Acesso rápido

Contexto:
	Dado que já me cadastrei no aplicativo
	E salvei um contato

Cenário: Posso visualizar as informações do contato cadastrado
	Dado que estou na home do aplicativo
	Quando eu tocar na foto do meu contato
	E clicar no ícone de informações
	Então devo ver o telefone do meu contato

Cenário: Posso efetuar chamadas utilizando o acesso rápido
   Dado que estou na home do aplicativo
   Quando eu tocar na foto do meu contato
   E clicar no ícone para chamadas rápidas
   Então devo ver a ligação sendo efetuada

 Cenario: Posso efetuar chamadas utilizando o acesso rápido
   Dado que estou na home do aplicativo
   Quando eu tocar na foto do meu contato
   E clicar no ícone de conversa
   Então devo ver o histórico de conversa




