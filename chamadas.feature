Funcionalidade:
    Limpar o histórico de chamadas

Contexto:
	Dado que já me cadastrei no aplicativo
	E salvei um contato
	E efetuei alguma ligação

Cenário: Posso excluir as chamadas efetuadas/recebidas do aplicativo
	Dado que estou na home do aplicativo
	Quando tocar na aba de chamadas
	E clicar no icone de informações
	E selecionar a opção Limpar chamadas
	E confirmar a exclusão do histórico de chamadas 
	Então não devo visualizar as chamadas efetuadas

Cenário: Posso cancelar a exclusão das chamadas efetuadas/recebidas do aplicativo
	Dado que estou na home do aplicativo
	Quando tocar na aba de chamadas
	E clicar no icone de informações
	E selecionar a opção Limpar chamadas
	E cancelar a exclusão do histórico de chamadas
	Então devo visualizar o histórico de chamadas 
